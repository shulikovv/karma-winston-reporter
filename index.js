var winston = require('winston');

var WinstonReporter = function (logger, config) {
    var winstonLogger = config ? new (winston.Logger)({transports: config.transports}) : winston;

    this.onBrowserComplete = function (browser) {
        var results = browser.lastResult;

        if (results.disconnected || results.error) {
            return;
        }

        winstonLogger.log('info', {
            successfulTests: results.success,
            failedTests: results.failed,
            testDuration: results.totalTime,
            testBrowser: browser.name
        });
    };
};

WinstonReporter.$inject = ['logger', 'config.winstonReporter'];

module.exports = {
    'reporter:winston': ['type', WinstonReporter]
};